# Xello UI Angular Test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

```
// install ng-cli
npm install -g @angular/cli
// install dependencies
npm install
// run server
ng serve
```
# Screenshot
 
![Scheme](http://codecharger.com/assets/xello-angular.png)
