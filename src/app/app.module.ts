import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ButtonListComponent } from './button-list/button-list.component';
import {RouterComponent} from './app.routes';
import { ButtonComponent } from './button-list/button/button.component';
import {ButtonService} from './button.service';
import { ClickoutsideDirective } from './clickoutside.directive';

@NgModule({
  declarations: [
    AppComponent,
    ButtonListComponent,
    ButtonComponent,
    ClickoutsideDirective,
  ],
  imports: [
    BrowserModule,
    RouterComponent
  ],
  providers: [ButtonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
