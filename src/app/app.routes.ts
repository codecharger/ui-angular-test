import {RouterModule, Routes} from '@angular/router';
import {ButtonListComponent} from './button-list/button-list.component';
import {NgModule} from '@angular/core';

const appRoutes: Routes = [
  {path: '', component: ButtonListComponent}
];

@NgModule( {
  imports: [RouterModule.forRoot(appRoutes)],
  exports:[RouterModule]
})

export class RouterComponent {}
