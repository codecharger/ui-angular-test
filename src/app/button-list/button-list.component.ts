import {Component, ElementRef, OnInit} from '@angular/core';

@Component({
  selector: 'app-button-list',
  templateUrl: './button-list.component.html',
  styleUrls: ['./button-list.component.scss']
})
export class ButtonListComponent implements OnInit {

  public isOpen: boolean;

  constructor() { }

  ngOnInit() {

  }

  onClick(event) {
    this.isOpen = event;
  }


}
