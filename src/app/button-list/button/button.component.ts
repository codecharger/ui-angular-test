import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() tooltipContent = '';
  @Input() isOpen = false;
  @Input() btnLabel = '';
  @ViewChild('localElement') localElement: ElementRef;
  public targetElem;
  constructor(private tooltipElement: ElementRef) {
  }

  ngOnInit() {

  }

  execFn() {
    this.isOpen = !this.isOpen;
  }

  // @Component({host: {
  //   '(document:click)': 'onClick($event)',
  // }
  // })
  // onClick(event) {
  //   if (!this.tooltipElement.nativeElement.contains(event.target))  {
  //     this.isOpen = false;
  //   }
  // }

  @HostListener('document:click', ['$event']) documentClick(event) {
    if (!this.tooltipElement.nativeElement.contains(event.target))  {
      this.isOpen = false;
    }
  }

  @HostListener('document:keydown', ['$event']) escapePressed(event: KeyboardEvent) {
    if (event.key === 'Escape') {
        this.isOpen = false;
    }
  }



}
